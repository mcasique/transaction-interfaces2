### Azure Config Maps Environment

| Name   | Value |
| ------ | ------ |
| Key    | ENV |
| Value  | dev |

| Name   | Value  |
| ------ | ------ |
| Key    | BASE_LOGS |
| Value  | /opt/apps |

### Azure Config Maps DB

| Name   | Value |
| ------ | ------ |
| Key    | ETRANSAC_USR |
| Value  | ETRANSAC |

| Name   | Value |
| ------ | ------ |
| Key    | ETRANSAC_PWD |
| Value  | Impuls0h19# |

| Name   | Value |
| ------ | ------ |
| Key    | DS_FULXG04_URL |
| Value  | jdbc:oracle:thin:@10.20.43.12:1521:FULXG04D |


### Azure Config Maps RMQ

| Name   | Value |
| ------ | ------ |
| Key    | RABBITMQ_TRANSACTION_USR |
| Value  | guest |

| Name   | Value |
| ------ | ------ |
| Key    | RABBITMQ_TRANSACTION_PWD |
| Value  | guest |

| Name   | Value |
| ------ | ------ |
| Key    |  RABBITMQ_TRANSACTION_HOST |
| Value  |  localhost |

| Name   | Value |
| ------ | ------ |
| Key    |  RABBITMQ_TRANSACTION_PORT |
| Value  |  5672 |
