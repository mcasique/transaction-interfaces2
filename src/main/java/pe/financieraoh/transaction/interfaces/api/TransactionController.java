package pe.financieraoh.transaction.interfaces.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.financieraoh.transaction.application.service.TransactionService;
import pe.financieraoh.transaction.domain.model.etransac.Transaction;
import pe.financieraoh.transaction.infrastructure.exception.ModelNotFoundException;
import pe.financieraoh.transaction.infrastructure.messaging.MessageProducer;
import pe.financieraoh.transaction.infrastructure.util.DateUtil;
import pe.financieraoh.transaction.infrastructure.util.MapperTransaction;
import pe.financieraoh.transaction.infrastructure.util.MessageRqConstant;
import pe.financieraoh.transaction.interfaces.dto.TransactionQueryRequest;
import pe.financieraoh.transaction.interfaces.dto.TransactionQueryResponse;

@RestController
@RequestMapping("/api/transactions")
@Api(value = "transactions")
public class TransactionController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionController.class);

	@Autowired
	private MessageProducer messageProducer;

	private final TransactionService transactionService;

	public TransactionController(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	/**
	 * Registra Transaction.
	 *
	 * @param message String
	 * @return message String
	 */
	@ApiOperation(value = "Registra una Transaccion", httpMethod = "POST", produces = "text/plain", consumes = "text/plain")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Transaccion creada"), @ApiResponse(code = 500, message = "No creado") })
	@PostMapping("/create")
	public ResponseEntity<String> create(
			@ApiParam(value = "Trama de Autorizacion Request", required = true) @RequestBody String message) {
		LOGGER.info("create Message Request: {}", message);
		String response = "";
		Transaction transaction = null;
		MapperTransaction app = MapperTransaction.getInstance();
		transaction = app.buildCreateTransaction(message);
		Long id = transactionService.create(transaction);
		if (id != 0) {
			messageProducer.sendMessage(message);
			transaction.setId(id);
			response = app.buildResponse(transaction);
		}
		LOGGER.info("create Message Response: {}", message);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}	

	@ApiOperation(value = "Registra una Transaccion Asincrona", httpMethod = "POST", consumes = "text/plain")
	@PostMapping("/createAsync")
	public ResponseEntity<Object> createAsync(
			@ApiParam(value = "Trama de Autorizacion Request", required = true) @RequestBody String message) {
		LOGGER.info("createAsync Message Request: {}", message);
		Transaction transaction = null;
		MapperTransaction app = MapperTransaction.getInstance();
		transaction = app.buildCreateTransaction(message);
		transactionService.create(transaction);
		Long id = transactionService.create(transaction);
		if (id != 0) {
			messageProducer.sendMessage(message);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@ApiOperation(value = "Validar una transaccion enviada")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Errores") })
	@PostMapping(value = "/validate")
	public ResponseEntity<String> validate(
			@ApiParam(value = "Trama de Autorizacion Request", required = true) @RequestBody String uuid) {
		LOGGER.info("validate Message Request: {}", uuid);
		String response = "NO";
		Transaction transaction = null;
		transaction = transactionService.findByUuid(uuid);
		if (transaction != null) {
			response = "OK";
		}
		LOGGER.info("validate Message Response: {}", response);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@ApiOperation(value = "Listado de Transacciones Enviadas", response = TransactionQueryResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Resultados"),
			@ApiResponse(code = 404, message = "No hay registros devueltos") })
	@PostMapping(value = "/list", produces = "application/json")
	public ResponseEntity<List<TransactionQueryResponse>> list(
			@ApiParam(value = "Trama de Autorizacion Request", required = true) @RequestBody TransactionQueryRequest filter)
		{

		LOGGER.info("list Message Request: {}", filter);

		Date transactionFromHour;
		Date transactionToHour;
		List<Transaction> listTransaction;
		List<TransactionQueryResponse> listado = null;

		transactionFromHour = DateUtil.convertStringToDate(
				filter.getTransactionDate().concat(filter.getTransactionFromHour()), DateUtil.PATTERN_DATE_ISO);
		transactionToHour = DateUtil.convertStringToDate(
				filter.getTransactionDate().concat(filter.getTransactionToHour()), DateUtil.PATTERN_DATE_ISO);
		listTransaction = transactionService.findByRangesHours(transactionFromHour, transactionToHour);
		listado = new ArrayList<>();
		for (Transaction transaction : listTransaction) {
			TransactionQueryResponse query = new TransactionQueryResponse();
			query.setMessageType(MessageRqConstant.MSG_RESPONSE_CODE);
			query.setUuid(transaction.getUuid());
			query.setTransactionDate(DateUtil.convertDateToStringYYYYMMDDHHMMSS(transaction.getTrxDateTime()));
			query.setResponseCode(MessageRqConstant.SUCCESS_RESPONSE);
			listado.add(query);
		}		
		if (!listado.isEmpty()) {
			LOGGER.info("list Message Response: {}", listado);
			return ResponseEntity.ok().body(listado);
		}else{
			LOGGER.info("list Message Response: {}", "No hay registros devueltos");
			throw new ModelNotFoundException("No hay registros devueltos");
		}
	}

}
