package pe.financieraoh.transaction.interfaces.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Datos de la Transaccion Autorizada enviada :")
public class TransactionQueryResponse implements Serializable{

	private static final long serialVersionUID = 1L;

	@NotNull
	@JsonProperty("msgType")
	@ApiModelProperty(notes = "Tipo de Mensaje" ,dataType="String" ,  required=true , example="R")
    private String messageType;
	
	
	@NotNull
	@JsonProperty("responseCode")
	@ApiModelProperty(notes = "Codigo de Respuesta" ,dataType="String" ,  required=true , example="00")
	private String responseCode;

	@NotNull
	@JsonProperty("uuid")
	@ApiModelProperty(notes = "UUID" ,dataType="String" ,  required = true ,example="c0d852ae-5cd2-11")
	private String uuid;
	
	@NotNull
	@JsonProperty("transactionDate")
	@ApiModelProperty(notes = "Fecha Hora de la Transaccion YYYYMMDD HHMMSS" ,dataType="String" ,  required=true , example="20190404 200100")
	private String transactionDate;

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}



	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	
}
