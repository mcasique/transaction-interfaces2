package pe.financieraoh.transaction.interfaces.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "Campos necesarios para filtrar la consulta de Transacciones ")
@JsonPropertyOrder({ "transactionDate" , "transactionFromHour" , "transactionToHour"})
public class TransactionQueryRequest implements Serializable{
	private static final long serialVersionUID = 1L;

	@NotNull
	@JsonProperty("date")
	@ApiModelProperty(notes = "Fecha de la Transaccion YYYYMMDD" ,dataType="String" ,  required=true ,example="20190412")
    private String transactionDate;
	
	@NotNull
	@JsonProperty("fromHour")
	@ApiModelProperty(notes = "Fecha de la Transaccion (24hrs) HHMMSS" ,dataType="String" ,  required=true  ,example="220300")
	private String transactionFromHour;
	
	@NotNull
	@JsonProperty("toHour")
	@ApiModelProperty(notes = "Fecha de la Transaccion (24hrs) HHMMSS" ,dataType="String" ,  required=true  ,example="230300")
	private String transactionToHour;

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionFromHour() {
		return transactionFromHour;
	}

	public void setTransactionFromHour(String transactionFromHour) {
		this.transactionFromHour = transactionFromHour;
	}

	public String getTransactionToHour() {
		return transactionToHour;
	}

	public void setTransactionToHour(String transactionToHour) {
		this.transactionToHour = transactionToHour;
	}
	

}
