package pe.financieraoh.transaction.infrastructure.etransac.repository;

import org.springframework.stereotype.Repository;

import pe.financieraoh.transaction.domain.model.etransac.Transaction;
import pe.financieraoh.transaction.domain.repository.TransactionRepository;

@Repository
public interface TransactionRepositoryImpl extends BaseRepository<Transaction, Long>, TransactionRepository {
}