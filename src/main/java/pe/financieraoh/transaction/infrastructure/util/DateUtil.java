package pe.financieraoh.transaction.infrastructure.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import pe.financieraoh.transaction.infrastructure.exception.ModelNotFoundException;

public class DateUtil {
	/**
	 * Constante para dd/MM/yyyy
	 */
	public static final String PATTERN_DATE_DD_MM_YYYY = "dd/MM/yyyy";
	/**
	 * Este es el patron estandar para enviar GregorianCalendar como cadenas, de
	 * forma interna. Por lo regular en mensajes de JMS.
	 */
	public static final String PATTERN_DEFAULT = "dd/MM/yyyy";
	/**
	 * Constante para fechas con patron yyyyMMdd
	 * */
	public static final String PATTERN_YYYYMMDD = "yyyyMMdd";
	/**
	 * Constante para fechas con patron aaaamm
	 * */
	public static final String PATTERN_YYYYMM = "yyyyMM";
	/**
	 * Constante para fechas con patron aammdd
	 * */
	public static final String PATTERN_YYMMDD = "yyMMdd";

	/**
	 * Constante para fechas con patron dMMyy
	 * */
	public static final String PATTERN_DMMYY = "dMMyy";

	/**
	 * Constante para fechas con patron dMMyyyy
	 **/
	public static final String PATTERN_DMMYYYY = "dMMyyyy";

	/**
	 * Constante para fechas con patron ddMMyyyy
	 **/
	public static final String PATTERN_DDMMYYYY = "ddMMyyyy";
	/**
	 * Constante para fechas con patron mmaadd
	 * */
	public static final String PATTERN_MMYYDD = "MMyydd";
	/**
	 * Constante para fechas con patron ddmmaa
	 * *
	public static final String PATTERN_DDMMYY = "ddMMyy";
	/**
	 * Constante para fechas con patron mmddaa dmmaaaa aammaaaa
	 * */
	public static final String PATTERN_MMDDYY = "MMddyy";
	/**
	 * Constante para fecha corta con patron ddmm
	 * */
	public static final String PATTERN_DDMM = "ddMM";
	/**
	 * Constante para fecha corta con patron mmdd
	 * */
	public static final String PATTERN_MMDD = "MMdd";
	/**
	 * Constante para fecha corta con patron dd
	 * */
	public static final String PATTERN_DD = "dd";
	/**
	 * Constante para fecha corta con patron MM
	 * */
	public static final String PATTERN_MM = "MM";
	/**
	 * Constante para fecha corta con patron yy
	 * */
	public static final String PATTERN_YY = "yy";
	/**
	 * Constante para fecha corta con patron YYYY
	 * */
	public static final String PATTERN_YYYY = "yyyy";

	/**
	 * Constante con el formato de fechas que incluye hora dd/MM/yyyy
	 * HH:mm:ss,SSS
	 */
	public static final String PATTERN_DD_MM_YYYY_TIME = "dd/MM/yyyy HH:mm:ss,SSS"; //$NON-NLS-1$
	public static final String PATTERN_DDMMYYYYTIME = "ddMMyyyyHHmmss"; //$NON-NLS-1$
	
	public static final String PATTERN_DATE_ISO = "yyyyMMddHHmmss";
	
	
	/**
	 * Constante con el formato de fechas que incluye hora dd/MM/yyyy
	 * HH:mm:ss,SSS
	 */
	public static final String PATTERN_YYYY_MM_DD_TIME = "yyyy/MM/dd HH:mm:ss"; //$NON-NLS-1$

	/**
	 * Constante con el formato de fechas que incluye hora dd/MM/yyyy
	 * HH:mm:ss,SSS
	 */
	public static final String PATTERN_TIME_HHMMSS = "HHmmss"; //$NON-NLS-1$

	/** Constante con el formato de fechas que incluye hora hh:mm a */
	public static final String PATTERN_TIME_HHMM_A = "hh:mm a"; //$NON-NLS-1$

	/** Constante con el formato de fechas que incluye hora hh:mm:ss a */
	public static final String PATTERN_TIME_HHMMSS_A = "hh:mm:ss a"; //$NON-NLS-1$
	
	/**
	 * Constante con el formato de fechas que incluye hora dd/MM/yyyy
	 * HH:mm:ss,SSS
	 */
	public static final String PATTERN_YYYY_MM_DD_TIME_MILLI = "yyyy-MM-dd HH:mm:ss.SSSSSS"; //$NON-NLS-1$
	
	public static final String PATTERN_YYYY_MM_DD_TIMESTAMP = "yyyy-MM-dd HH:mm:ss"; //$NON-NLS-1$
	
	public static final String PATTERN_YYYY_MM_DD = "yyyy-MM-dd";
	
	public static final String PATTERN_YYYY_MM_DD_HH_MM_SS = "yyyyMMdd HHmmss";
	
	/**
	 * Constante para dd/MM/yyyy
	 */
	public static final String PATTERN_DATE_YYYY_MM_DD = "yyyy/MM/dd";
	
	public static final String PATTERN_HH_MM_SS = "HH:mm:ss"; //$NON-NLS-1$
	
	/**
	 * Constante con el formato de fechas que incluye hora dd/MM
	 * HH:mm
	 */
	public static final String PATTERN_DD_MM_TIME = "dd/MM HH:mm"; //$NON-NLS-1$

	private DateUtil() {
		super();
	}

	public static String getCurrentDateAsString(String format){
		String actualDateAsString = null;
		Date actualDate = null;
		SimpleDateFormat dateFormat = null;

		actualDate = new Date();
		dateFormat = new SimpleDateFormat(format);
		actualDateAsString = dateFormat.format(actualDate);

		return actualDateAsString;
	}

	public static Date convertStringToDate(String date, String format){
		Date fileDate = null;
		SimpleDateFormat dateFormat = null;		
		try {
			dateFormat = new SimpleDateFormat(format);
			fileDate = dateFormat.parse(date);
		} catch (ParseException e) {
			throw new ModelNotFoundException(e.getMessage());
		}
		return fileDate;
	}

	public static String getLongDateAsString(long date, String format){
		String actualDateAsString = null;
		Date actualDate = null;
		SimpleDateFormat dateFormat = null;

		actualDate = new Date(date);
		dateFormat = new SimpleDateFormat(format);
		actualDateAsString = dateFormat.format(actualDate);

		return actualDateAsString;
	}
	
	public static String convertDateToString(Date fecha){
		DateFormat df = new SimpleDateFormat(PATTERN_DATE_DD_MM_YYYY);
		return df.format(fecha);
	}
	
	public static String convertDateToStringYYYYMMDD(Date fecha){
		DateFormat df = new SimpleDateFormat(PATTERN_YYYY_MM_DD);
		return df.format(fecha);
	}
	
	
	public static boolean compareTwoDates(String fechaActual, String fechaNueva){
		boolean bool = false;
		
		if(convertStringToDate(fechaActual, PATTERN_YYYY_MM_DD_TIME_MILLI ).before(
			   convertStringToDate(fechaNueva, PATTERN_YYYY_MM_DD_TIME_MILLI))){
				bool = true;
			}		
		return bool;
	}
	
	public static String convertDateToStringYYYYMMDDHHMMSS(Date fecha){
		DateFormat df = new SimpleDateFormat(PATTERN_YYYY_MM_DD_HH_MM_SS);
		return df.format(fecha);
	}
	
	public static String convertDateToStringMMDDHHMM(Date fecha){
		DateFormat df = new SimpleDateFormat(PATTERN_DD_MM_TIME);
		return df.format(fecha);
	}
	

}
