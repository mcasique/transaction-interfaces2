package pe.financieraoh.transaction.infrastructure.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.financieraoh.transaction.domain.model.etransac.Transaction;
import pe.financieraoh.transaction.infrastructure.exception.ModelNotFoundException;

public class MapperTransaction {

	private static final MapperTransaction _INSTANCE = new MapperTransaction();
	private static final Logger LOGGER = LoggerFactory.getLogger(MapperTransaction.class);

	private MapperTransaction() {
	}

	public static MapperTransaction getInstance() {
		return _INSTANCE;
	}

	public Transaction buildCreateTransaction(String message) {
		Transaction transaction;
		MessageUtil ppm;
		String messageReqType;
		String uuid;
		String bank;
		String application;
		String user;
		String trace;
		String charset;
		String filler;
		try {
			transaction = new Transaction();
			ppm = MessageUtil.getInstance();
			messageReqType = ppm.getSubString(MessageRqConstant.MSG_TYPE_REQUEST, message).trim();
			uuid = ppm.getSubString(MessageRqConstant.MSG_UUID, message).trim();
			bank = ppm.getSubString(MessageRqConstant.MSG_BANK_CODE, message).trim();
			application = ppm.getSubString(MessageRqConstant.MSG_APPLICATION_CODE, message).trim();
			user = ppm.getSubString(MessageRqConstant.MSG_USER, message).trim();
			trace = ppm.getSubString(MessageRqConstant.MSG_TRACE, message).trim();
			charset = ppm.getSubString(MessageRqConstant.MSG_CHARSET, message).trim();
			filler = ppm.getSubString(MessageRqConstant.MSG_FILLER, message).trim();
			LOGGER.info(
					"Message header: messageReqType: {}, uuid: {}, bank: {}, application: {}, user: {}, trace: {}, charset: {}, filler: {}",
					messageReqType, uuid, bank, application, user, trace, charset, filler);
			String messageType = ppm.getSubString(MessageRqConstant.MSG_TYPE, message).trim();
			String cardNumber = ppm.getSubString(MessageRqConstant.MSG_CARD_NUMBER, message).trim();
			String transactionCode = ppm.getSubString(MessageRqConstant.MSG_TRX_CODE, message).trim();
			String accountType = ppm.getSubString(MessageRqConstant.MSG_ACCOUNT_TYPE, message).trim();
			String reverse = ppm.getSubString(MessageRqConstant.MSG_REVERSE, message).trim();
			String amountLocal = ppm.getSubString(MessageRqConstant.MSG_LOCAL_AMOUNT, message).trim();
			String amountOriginal = ppm.getSubString(MessageRqConstant.MSG_ORIGINAL_AMOUNT, message).trim();
			String amountCollection = ppm.getSubString(MessageRqConstant.MSG_COLLECTION_AMOUNT, message).trim();
			String transactionDate = ppm.getSubString(MessageRqConstant.MSG_TRX_DATE, message).trim();
			String conversionRate = ppm.getSubString(MessageRqConstant.MSG_CONVERSION_RATE, message).trim();
			String transactionNumber = ppm.getSubString(MessageRqConstant.MSG_TRX_NUMBER, message).trim();
			String transactionHour = ppm.getSubString(MessageRqConstant.MSG_TRX_DATE_HOUR, message).trim();
			String transactionDateOnly = ppm.getSubString(MessageRqConstant.MSG_TRX_DATE_ONLY, message).trim();
			LOGGER.info(
					"Message body: messageType: {}, cardNumber: {}, transactionCode: {}, accountType: {}, reverse:{}, amountLocal: {}, amountOriginal: {}, amountCollection: {}, transactionDate: {}, conversionRate: {}, transactionNumber: {}, transactionHour: {}, transactionDateOnly: {}",
					messageType, cardNumber, transactionCode, accountType, reverse, amountLocal, amountOriginal,
					amountCollection, transactionDate, conversionRate, transactionNumber, transactionHour,
					transactionDateOnly);
			transaction.setOperacion(trace);
			transaction.setTrace(trace);
			transaction.setData(message);
			transaction.setTrxDateTime(DateUtil.convertStringToDate(transactionDate, DateUtil.PATTERN_DATE_ISO));
			transaction.setTrxDate(DateUtil.convertStringToDate(transactionDateOnly, DateUtil.PATTERN_YYYYMMDD));
			transaction.setTrxHour(DateUtil.convertStringToDate(transactionHour, DateUtil.PATTERN_TIME_HHMMSS));
			transaction.setUuid(uuid);
			transaction.setCreatedBy(user);
		} catch (Exception e) {
			throw new ModelNotFoundException(e.getMessage());
		}
		return transaction;
	}

	public String buildResponse(Transaction transaction) {
		StringBuilder data2 = new StringBuilder("");
		data2.append(MessageUtil.convertAlphanumericAString(MessageRqConstant.MSG_RESPONSE_CODE,
				MessageRqConstant.MSG_TYPE_REQUEST));
		data2.append(MessageUtil.convertAlphanumericAString(transaction.getUuid(), MessageRqConstant.MSG_UUID));
		data2.append(MessageUtil.convertAlphanumericAString(MessageRqConstant.SUCCESS_RESPONSE,
				MessageRqConstant.MSG_RES_CODE));
		data2.append(MessageUtil.convertNumericAString(transaction.getTrace(), MessageRqConstant.MSG_TRACE));
		data2.append(MessageUtil.convertAlphanumericAString(transaction.getOperacion(), MessageRqConstant.MSG_FILLER));
		return data2.toString();
	}

}