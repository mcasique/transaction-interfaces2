package pe.financieraoh.transaction.infrastructure.util;

public class MessageRqConstant {
	private MessageRqConstant() {
	    }
	  
	public static final int MSG_TYPE_REQUEST = 1;
	public static final int MSG_UUID = 16;
	public static final int MSG_BANK_CODE = 3;
	public static final int MSG_APPLICATION_CODE = 10;
	public static final int MSG_USER = 20;
	public static final int MSG_TRACE = 6;
	public static final int MSG_CHARSET = 1;
	public static final int MSG_FILLER = 20;
	public static final int MSG_TYPE = 4;
	public static final int MSG_CARD_NUMBER = 19;
	public static final int MSG_TRX_CODE = 2;
	public static final int MSG_ACCOUNT_TYPE = 4;
	public static final int MSG_REVERSE = 1;
	public static final int MSG_LOCAL_AMOUNT = 14;
	public static final int MSG_ORIGINAL_AMOUNT = 14;
	public static final int MSG_COLLECTION_AMOUNT = 14;
	public static final int MSG_TRX_DATE = 14;
	public static final int MSG_CONVERSION_RATE = 11;
	public static final int MSG_TRX_NUMBER = 6;
	public static final int MSG_TRX_DATE_HOUR = 6;
	public static final int MSG_TRX_DATE_ONLY = 8;
	public static final int MSG_RES_CODE = 2;
	
	//INICIO: JCPEREZ | TRXDETAIL
	public static final int MSG_V_TO = 4;
	public static final int MSG_PROCESS_MODE = 1;
	public static final int MSG_DATE_APPLICATION = 8;
	public static final int MSG_MCC = 4;
	public static final int MSG_CODE_COUNTRY_ADQ = 3;
	public static final int MSG_CODE_COUNTRY_TC = 3;
	public static final int MSG_CODE_COUNTRY_TRANSMITTER = 3;
	public static final int MSG_ENTRY_MODE = 4;
	public static final int MSG_PRINCIPAL_CARD = 19;	
	public static final int MSG_PTO_CONDITION_SALE = 2;
	public static final int MSG_MCG_ACT_CIO = 4;
	public static final int MSG_ID_ADQ = 11;
	public static final int MSG_ID_TRANSMITTER = 11;
	public static final int MSG_AUTHORIZATION = 6;
	public static final int MSG_COD_RPTA = 3;
	public static final int MSG_REASON_ANSWER = 80;
	public static final int MSG_SERVICE_CODE = 3;
	public static final int MSG_ID_TERMINAL = 8;
	public static final int MSG_COMMERCE_CODE = 15;
	public static final int MSG_COMMERCE_NAME = 25;
	public static final int MSG_COMMERCE_LOCALITY = 13;
	public static final int MSG_COUNTRY_ORIGIN = 3;
	public static final int MSG_TYPE_PROD_TC = 3;
	public static final int MSG_NAME_TC_BD = 25;
	public static final int MSG_CLIENT_NAME = 25;
	public static final int MSG_COD_CURRENCY_TRX = 3;
	public static final int MSG_COD_CURRENCY_TH = 3;
	public static final int MSG_BALANCE_HAND = 14;
	public static final int MSG_GPS_POSITION = 14;
	public static final int MSG_CUPO_CARD = 12;
	public static final int MSG_ID_CTA_ORIGIN = 23;
	public static final int MSG_ID_CTA_TRANSFER = 23;
	public static final int MSG_BIN = 6;
	public static final int MSG_COD_BIN_LIQUIDATOR = 6;
	public static final int MSG_CARD_SITUATION = 10;
	public static final int MSG_VIRTUAL_TC = 1;
	public static final int MSG_TIT_P_ADIC_A = 1;
	public static final int MSG_CREDIT_LINE = 12;
	public static final int MSG_POINTS_MILES = 12;
	public static final int MSG_OFFICE_RADICATION_CARD = 6;
	public static final int MSG_CLIENT_CODE = 15;
	public static final int MSG_FIRST_NAME_CLIENT = 12;
	public static final int MSG_SECOND_NAME_CLIENT = 12;
	public static final int MSG_FIRST_SURNAME_CLIENT = 12;
	public static final int MSG_SECOND_SURNAME_CLIENT = 12;
	public static final int MSG_ID_CLIENT = 12;
	public static final int MSG_BIRTH_DATE = 8;
	public static final int MSG_BLANK = 12;
	public static final int MSG_DIRECTION_CORRESPONDENCE = 35;
	public static final int MSG_EMAIL = 35;
	public static final int MSG_RECIDENCE_PHONE = 12;
	public static final int MSG_OFFICE_PHONE = 12;
	public static final int MSG_CORRESPONDENCE_PHONE = 12;
	public static final int MSG_MOBILE_PHONE = 12;
	public static final int MSG_ALERT_ORIGIN = 3;
	public static final int MSG_BANKNET_REFERENCE = 12;
	public static final int MSG_UCAF = 3;
	public static final int MSG_FREE_FIELD_BANK = 25;
	public static final int MSG_FRAUD_INDICATOR = 1;
	public static final int MSG_COUNTRY_CARD = 22;
	public static final int MSG_BIN_VTO = 10;
	public static final int MSG_ADDITIONAL_POSINFORMATION = 22;
	public static final int MSG_COMMERCE_CLASSIFIER = 18;
	public static final int MSG_LOCAL_OR_INTERNATIONAL_EPJ = 1;
	public static final int MSG_EPJ_RANGE_AMOUNT = 2;
	public static final int MSG_EPJ_MCG = 4;
	public static final int MSG_RESERVED = 116;
	//FIN: JCPEREZ | TRXDETAIL
		
	public static final String MSG_RESPONSE_CODE = "R";
	public static final String SUCCESS_RESPONSE = "00";
}
