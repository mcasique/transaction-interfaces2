package pe.financieraoh.transaction.infrastructure.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableRabbit
public class SpringAmqpConfig {

    public static final String QUEUENAME = "financieraoh-transaction";
    public static final String EXCHANGENAME = "financieraoh-transaction-exchange";
    public static final String ROUTINGKEY = "foh.trx";

    @Bean
    Queue queue() {
        return new Queue(QUEUENAME, true);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(EXCHANGENAME);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTINGKEY);
    }   

}