package pe.financieraoh.transaction.domain.model.etransac;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "TRANSACTION" ,indexes = { @Index(name = "IDX_MYIDX1", columnList = "DATE_TIME_TRX") })
public class Transaction extends BaseEntityAudit{

	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		@Column(name="UUID", unique=true , length=16)
	    private String uuid;
	  
		@Column(name="TRACE",  length=6)
	    private String trace;
		
		@Column(name="OPERACION",  length=20)
	    private String operacion;		
		
	    @Column(name="DATA", columnDefinition="CLOB NOT NULL") 
	    @Lob 
	    private String data;
		
		   @Column(name = "DATE_TIME_TRX")	
		    @Temporal(TemporalType.TIMESTAMP)
		    private Date trxDateTime;
		   
		   @Column(name = "TRX_DATE_TRX")
		    @Temporal(TemporalType.DATE)
		    private Date trxDate;
		   
		   
		   @Column(name = "TRX_HOUR_TRX")
		    @Temporal(TemporalType.TIME)
		    private Date trxHour;

		public Date getTrxDateTime() {
			return trxDateTime;
		}

		public void setTrxDateTime(Date trxDateTime) {
			this.trxDateTime = trxDateTime;
		}

		public Date getTrxDate() {
			return trxDate;
		}

		public void setTrxDate(Date trxDate) {
			this.trxDate = trxDate;
		}

		public Date getTrxHour() {
			return trxHour;
		}

		public void setTrxHour(Date trxHour) {
			this.trxHour = trxHour;
		}

		public String getUuid() {
			return uuid;
		}

		public void setUuid(String uuid) {
			this.uuid = uuid;
		}

		public String getTrace() {
			return trace;
		}

		public void setTrace(String trace) {
			this.trace = trace;
		}

		public String getOperacion() {
			return operacion;
		}

		public void setOperacion(String operacion) {
			this.operacion = operacion;
		}

		public String getData() {
			return data;
		}

		public void setData(String data) {
			this.data = data;
		}
	    

}
