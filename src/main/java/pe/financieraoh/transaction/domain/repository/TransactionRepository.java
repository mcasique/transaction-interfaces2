package pe.financieraoh.transaction.domain.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import pe.financieraoh.transaction.domain.model.etransac.Transaction;

public interface TransactionRepository {
	  List<Transaction> findAll();
	  Transaction save(Transaction transaction);
	  Transaction findByUuid(String uuid);
	  Transaction findById(String id);
	  @Query("select t from Transaction t where t.trxDateTime BETWEEN :transactionFromHour and :transactionToHour")
	  List<Transaction> findByRangesHours(Date transactionFromHour, Date transactionToHour);
	  void deleteByUuid(String uuid);
}
