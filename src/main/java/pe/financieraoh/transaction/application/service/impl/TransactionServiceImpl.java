package pe.financieraoh.transaction.application.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.financieraoh.transaction.application.service.TransactionService;
import pe.financieraoh.transaction.domain.model.etransac.Transaction;
import pe.financieraoh.transaction.domain.repository.TransactionRepository;

@Service
public class TransactionServiceImpl implements TransactionService {

	private final TransactionRepository transactionRepository;
	
	@Autowired
	public TransactionServiceImpl(TransactionRepository transactionRepository) {
		this.transactionRepository = transactionRepository;
	}

	@Override
	public Long create(Transaction t) {	
		return transactionRepository.save(t).getId();
	}

	@Override
	public Transaction findByUuid(String uuid) {
		return transactionRepository.findByUuid(uuid);
	}

	@Override
	public List<Transaction> findByRangesHours(Date transactionFromHour, Date transactionToHour) {
		return transactionRepository.findByRangesHours(transactionFromHour, transactionToHour);
	}

}
