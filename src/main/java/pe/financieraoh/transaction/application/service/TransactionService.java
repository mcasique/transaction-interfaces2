package pe.financieraoh.transaction.application.service;

import java.util.Date;
import java.util.List;

import pe.financieraoh.transaction.domain.model.etransac.Transaction;


public interface TransactionService extends BaseService<Transaction, Long> {

	/**
	 * Find all entity by the ranges hours
	 * 
	 * @param transactionDate
	 * @param transactionFromHour
	 * @param transactionToHour
	 * @return the list entity
	 */
	List<Transaction> findByRangesHours(Date transactionFromHour, Date transactionToHour);
	
	/**
	 * Find an entity by the uuid
	 * 
	 * @param uuid
	 * @return an entity
	 */
	Transaction findByUuid(String id);
}
